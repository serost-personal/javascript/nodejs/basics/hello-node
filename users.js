const path = require("path");
const fs = require("fs");
const dbMockFilepath = path.join(".", "mockdb.json");

let dbMockContent = null;

const connectToUsersDB = () => {
  try {
    content = fs.readFileSync(dbMockFilepath, "utf-8");
  } catch {
    throw new Error(
      'Failed to connect to mock db! (try running "npm run init_mock_db")'
    );
  }

  try {
    dbMockContent = JSON.parse(content);
  } catch (e) {
    throw new Error("Failed to open mockDB:\n" + e);
  }
};

// assume that database "connection" is valid
const getUser = (id) => {
  return dbMockContent.users[id];
};

const getUsers = () => {
  return dbMockContent.users;
};

module.exports = {
  connectToUsersDB,
  getUser,
  getUsers,
};
