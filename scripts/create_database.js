#!/usr/bin/env node

/* Why this form of "shebang is used?
 * Here: https://stackoverflow.com/questions/20638520/appropriate-hashbang-for-node-js-scripts
/

/*
 * could've just use "cp templates/mockdb.json ./mockdb.json",
 * but lets make things a little more fun...
 */
const fs = require("fs");
const path = require("path");

/* path.join adds separator to a file
 * "/" in UNIX systems
 * "\\" in WINDOWS systems
 * ... and possibly others, of which im unaware of
 */
const projectRoot = path.join(__dirname, "..");

const mockdbFilepath = path.join(projectRoot, "templates", "mockdb.json");
const mockdbCopypath = path.join(projectRoot, "mockdb.json");

// callback is run, when file is copied (or error thrown, perhaps missing permission)
// since nodejs is async nonblocking runtime, it will try do to something like
// following C snippet:
//  int fd = open("file.txt", O_NONBLOCK, ...
// and then do some more usefull things, aside from you know.. waiting..
fs.copyFile(mockdbFilepath, mockdbCopypath, (err) => {
  // same as if (err != undefined) { ...
  if (!err) {
    // unicode, who would've thought...
    console.log(`Success 🎉!\nDatabase mocked at: ${mockdbCopypath}`);
    return;
  }
  // use the funny quotes ` to mark format string
  console.error(
    `Failed to copy mock db file: ${mockdbFilepath} -> ${mockdbCopypath}\n` +
      `${err.message}`
  );
});
