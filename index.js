/*
    This application does following:

    1. Create endpoint to add / remove / read users.
        whereas users is just a list, not an actual db connection.
*/

const express = require("express");
const db = require("./users");

const app = express();

// if process.env.PORT is undefined, use 5000
const expressPort = process.env.PORT | 5000;

app.get("/api/users", (req, res) => {
  const id = req.query.id;
  res.setHeader("Content-Type", "text/json");

  if (!id) {
    res.send(db.getUsers());
  }

  res.send(db.getUser(id));
});

app.listen(expressPort, () => {
  db.connectToUsersDB();
  console.log(`Running express at port ${expressPort}`);
});
